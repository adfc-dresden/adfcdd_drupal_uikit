<?php

/**
 * @file
 * adfcdduk theme system, which controls the output of adfcdduk.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by UIkit themes.
 */

/**
 * Implements hook_theme().
 */
/* -- Delete this line if you want to use this function
function adfcdduk_theme($existing, $type, $theme, $path) {
}
// */
