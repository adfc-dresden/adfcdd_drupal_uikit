<?php

/**
 * @file
 * Modify structured content arrays.
 *
 * These hooks are called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * content structure has been built.
 *
 * If the theme wishes to act on the rendered HTML of the content rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_HOOK().
 *
 * @see \Drupal\Core\Render\RendererInterface
 * @see \Drupal\Core\Render\Renderer
 */

/**
 * Implements hook_theme_suggestions_alter().
 */
/* -- Delete this line if you want to use this function
function adfcdduk_theme_suggestions_alter(array &$suggestions, array $variables) {
}
// */

/**
 * Metaheader search form button into icon
 */

function adfcdduk_form_search_api_form_alter(&$form, &$form_state) {
  $form['actions']['submit']['#value'] = html_entity_decode('&#xf002;');
  $form['actions']['submit']['#attributes']['class'][] = 'fa-solid fa-magnifying-glass';
}

function adfcdduk_theme_suggestions_fieldset_alter(array &$suggestions, array $variables, $hook)
{
    if (isset($variables['element']['#id'])) {
        $id = str_replace("-", "_", $variables['element']['#id']);
        $suggestions[] = $hook . '__' . $id;
    }
}