# ADFC Dresden Drupal Installation

## Installation im Webverzeichnis des Webhosters (all-inkl.com)
 - Den Inhalt dieses Repository in /www/htdocs/w012345678/drupaltest.adfc-dresden.de kopieren (per FTP oder per ``git clone'')
 - Drupal und alle Libraries (entsprechend package.json) mit `composer install` (ggf. `composer update`) installieren
 - Die MariaDB-Datenbank importieren, z.B. mit `mysql -p -u d035b256 d035b256` und `source ./sql.sql`
 - Eine Datei sites/default/settings.php muss zwingend erstellt werden. Einfach default.settings.php kopieren in settings.php und folgende Einstellungen am Ende von settings.php einfügen einfügen


## settings.php
 $databases['default']['default'] = [
  'database' => 'd035b256',
  'username' => 'd035b256',
  'password' => 'XXXXXXXX',
  'host' => 'localhost',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
  'init_commands' => [
    'isolation_level' => 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;',
  ],
];
$config['system.logging']['error_level']='verbose';
$settings['config_sync_directory'] = 'sites/default/files/config_TODOTODOTODOTODOTODO/sync'; // dieses Verzeichnis muss (leer) erstellt werden!
$settings['hash_salt'] = 'something long TODOTOTOTODO';

## Aufbau dieses Repositorys
- wesentlich ist composer.json (TODO ggf. auch composer.lock) für die Installation von Drupal Core und die Libraries
- für eine benötigte Library jquery-ui-slider-pips war kein Composer-Paket verfügbar, daher ist eine Kopie davon unter libraries\jquery-ui-slider-pips
- Das adfcdd-Theme unter themes/custom/adfcdd ist eine angepasste Kopie des [Zurb Foundation-Theme](https://github.com/foundation/foundation-sites/tree/v6.7.5/dist). Jedoch nicht als Subtheme. Das Base Theme ist Stable (v8) vom Drupal 9, wie bei Foundation. Foundation JS und CSS liegt unter /foundation. Die von Foundation benötigte Library motion-ui.js liegt unter /motion-ui

# Notizen zum Aufräumen/Entwurf


cp adfcdd.dvlp adfcdd.uk

python ../git-filter-repo.py --path themes/custom/adfcdduk --path .gitignore --path composer.json --path composer.lock --force





sites/default/services.yml bearbeitet -> twig debug enable


--to-subdirectory-filter 





Funktioniert nicht....
mysqlimport --verbose -p -u d035b256 d035b256 adfcdd_2022-12-02_10-59-51.sql

Funktioniert...
mysql -p -u d035b256 d035b256
source ./sql.sql

mkdir sites/default/files
mkdir sites/default/files/config_WOb3YqeRQTryfj7ndew383kFk2NhfLXfbEZtHeqPvkSIPHd7k0yrBHk_MwULF1cPNbwyHhQ
mkdir sites/default/files/config_WOb3YqeRQTryfj7ndew383kFk2NhfLXfbEZtHeqPvkSIPHd7k0yrBHk_MwULF1cPNbwyHhQ/sync

sites/default/settings.php 






$databases['default']['default'] = [
  'database' => 'd035b256',
  'username' => 'd035b256',
  'password' => 'XXXX',
  'host' => 'localhost',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
  'init_commands' => [
    'isolation_level' => 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;',
  ],
];

//$config['system.logging']['error_level']='verbose';

$settings['config_sync_directory'] = 'sites/default/files/config_WOb3YqeRQTryfj7ndew383kFk2NhfLXfbEZtHeqPvkSIPHd7k0yrBHk_MwULF1cPNbwyHhQ/sync';

$settings['hash_salt'] = 'something long';

# Um Warnung zu unterdrücken (wegen Webformular File Upload)
$settings['file_private_path'] =  $app_root . '/../private_upload_tmp';


cmd /u (enable UTF16 mode)
cd C:\Users\nilsl\Desktop\adfc-drupal\files
dir /b /s > list
S&R "C:\Users\nilsl\Desktop\adfc-drupal\files\" with ""
notepad++ regex search [^a-z0-9_\-\.\r\n\\] for filenames (lines) with unsafe web characters




Fehlermeldung
Der Feed von Termine scheint aufgrund des Fehlers „DOMDocument cannot parse XML: Start tag expected, '<' not found“ nicht zu funktionieren. 


Es gibt einen Eintrag zum Modul node_export in der Tabelle system.schema. Das Modul ist aber nicht installiert. Weitere Informationen zu diesem Fehler.
Es gibt einen Eintrag zum Modul realistic_dummy_content in der Tabelle system.schema. Das Modul ist aber nicht installiert. Weitere Informationen zu diesem Fehler.
Es gibt einen Eintrag zum Modul realistic_dummy_content_api in der Tabelle system.schema. Das Modul ist aber nicht installiert. Weitere Informationen zu diesem Fehler.
Es gibt einen Eintrag zum Modul termine_extern in der Tabelle system.schema,Das Modul selbst ist aber nicht vorhanden. Weitere Informationen zu diesem Fehler.



To find unused twigs. First crawl the entire webpage to trigger twig compilation to php.
wget -mpEk drupaltest.base2.de

cd /www/htdocs/w0179a0e/drupaltest.base2.de
vendor/drush/drush/drush twig:unused --field=template /www/htdocs/w0179a0e/drupaltest.base2.de/themes/custom/adfcdd/



adfcdd theme based on foundation-sites
https://github.com/foundation/foundation-sites/tree/develop/dist
adfcdd/js und adfcdd/css enthalten alle Dateien aus o.g. Verzeichnis UND noch

motion-ui.min.js
what-input.js
iframe.css
motion-ui.min.css
fa_all.min.css
adfc_style.css
fa_all.min.css + fs/ Font Awesome


https://git.drupalcode.org/project/zurb-foundation/-/tree/8.x-6.x/images/foundation
adfcdd\img\foundation enthalten alle Dateien aus o.g. Verzeichnis


https://git.drupalcode.org/project/drupal/-/tree/9.2.7/core/themes/stable/templates
Die Dateien unter adfcdd/templates scheinen vom "stable"-Theme Drupal Core 9.2.7 zu kommen

https://git.drupalcode.org/project/zurb-foundation/-/blob/8.x-6.x/zurb_foundation.libraries.yml
adfcdd.libraries.yml



Bild-Uploadverzeichnis einstellen
http://drupaltest.base2.de/admin/structure/paragraphs_type/bild/fields/paragraph.bild.field_bild_in_bild